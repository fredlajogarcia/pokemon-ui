import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './PokemonUi-styles.js';
import estilos from './estilos';
import '@bbva-web-components/bbva-web-button-default/bbva-web-button-default.js';
import '@bbva-web-components/bbva-web-form-select/bbva-web-form-select.js';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<pokemon-ui></pokemon-ui>
```

##styling-doc

@customElement pokemon-ui
*/
export class PokemonUi extends LitElement {
  static get is() {
    return 'pokemon-ui';
  }

  // Declare properties
  static get properties() {
    return {
      name: { type: String, },
      limite: { type: String, },
      offset: { type: String, },
      pokemonId: { type: String, },
      base: { type: Number, },
      pokemon: { type: Object, },
      lista2: { type: Array, },
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.limite="151";
    this.offset="0";
    this.base= 180;
    this.name = 'Cells';
    this.pokemonId = '1';
    this.pokemon=new Object;
    this.pokemon.name="venusaur";
    
    this.pokemon.altura="20";
    this.pokemon.experiencia="236";
    this.pokemon.sprite="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/3.png";
    this.pokemon.spriteback="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/3.png";
    this.pokemon.shiny="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/3.png";
    this.pokemon.shinyback="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/3.png";
    this.pokemon.spritefemale="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/3.png";
    this.pokemon.spritebackfemale="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/3.png";
    this.pokemon.shinyfemale="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/3.png";
    this.pokemon.shinybackfemale="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/3.png";
    this.pokemon.artwork="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/3.png";

    this.pokemon.stats=[{"base_stat":"80","stat":{"name":"hp"}},
                        {"base_stat":"83","stat":{"name":"defense"}},
                        {"base_stat":"100","stat":{"name":"special-attack"}},
                        {"base_stat":"100","stat":{"name":"special-defense"}},
                        {"base_stat":"80","stat":{"name":"speed"}},
                        ]

    this.lista2=[{"name":"venusaur","url":"https://pokeapi.co/api/v2/pokemon/3/"},{"name":"charmander","url":"https://pokeapi.co/api/v2/pokemon/4/"},{"name":"charmeleon","url":"https://pokeapi.co/api/v2/pokemon/5/"}];
  }

  static get styles() {
    return [
      styles,estilos,
      getComponentSharedStyles('pokemon-ui-shared-styles','misEstilos')
    ];
  }

  // Define a template
  render() {
    return html`
      
      <div class="caja2 a_100" id="contenedor" style="width:1080px;">
        <div class="a_100 caja_vertical">
          <div class="caja2 a_100 box">
          <bbva-web-form-select class="a_40 " label="Generacion" @change=${this.setgeneracion} id="select2" >
              <bbva-web-form-option value="1">Primera Generacion</bbva-web-form-option>  
              <bbva-web-form-option value="2">Segunda Generacion</bbva-web-form-option> 
              <bbva-web-form-option value="3">Tercera Generacion</bbva-web-form-option> 
              <bbva-web-form-option value="4">Cuarta Generacion</bbva-web-form-option> 
              <bbva-web-form-option value="5">Quinta Generacion</bbva-web-form-option> 
              <bbva-web-form-option value="6">Sexta Generacion</bbva-web-form-option> 
              <bbva-web-form-option value="7">Septima Generacion</bbva-web-form-option> 
              <bbva-web-form-option value="8">Octaba Generacion</bbva-web-form-option> 
            </bbva-web-form-select>
          <bbva-web-button-default class="a_60" @click=${this.obtenerPokemon}> Buscar Pokemons</bbva-web-button-default>

          </div>
            
          <bbva-web-form-select class="a_100 hidden" label="Pokemon" @change=${this.setPoke} id="select" >
            <bbva-web-form-option value="L">DNI</bbva-web-form-option>  
          </bbva-web-form-select>
        
        </div>
        <div class=" a_100  caja2  hidden grey" id="sprites">
        <img src=${this.pokemon.sprite} alt="defecto" width="12%">
        <img src=${this.pokemon.spriteback} alt="defecto" width="12%">
        <img src=${this.pokemon.shiny} alt="defecto" width="12%">
        <img src=${this.pokemon.shinyback} alt="defecto" width="12%">
        <img src=${this.pokemon.spritefemale} alt="defecto" width="12%">
        <img src=${this.pokemon.spritebackfemale} alt="defecto" width="12%">
        <img src=${this.pokemon.shinyfemale} alt="defecto" width="12%">
        <img src=${this.pokemon.shinybackfemale} alt="defecto" width="12%">
        </div>
        <div class=" a_40 caja_vertical center hidden " id="art">
          <img src=${this.pokemon.artwork} alt="defecto" width="100%">
        </div>
        <div class="a_50 caja2   box radio hidden con_start " style="margin:10px;" id="stats">
            <div class="a_100 caja2"><h3>Estadisticas Base</h3> </div>
           
            <div class="a_30 h_20 caja_vertical right p_0_15 m_10_0" >
              ${this.pokemon.stats[0].stat.name}
              
            </div>
            <div class="a_70 h_20 caja_vertical grey p_0 m_10_0">
            <div class="barra" style="height:100%;width:${this.pokemon.stats[0].base_stat/this.base*100}%; background-color:${ this.pokemon.stats[0].base_stat/this.base >= 0.5 ? 'green' : 'gold'};">${this.pokemon.stats[0].base_stat}</div>
            </div>

            <div class="a_30 h_20 caja_vertical right p_0_15 m_10_0" >
              ${this.pokemon.stats[1].stat.name}
            </div>
            <div class="a_70 h_20 caja_vertical grey p_0 m_10_0">
            <div  class="barra" style="height:20px;width:${this.pokemon.stats[1].base_stat/this.base*100}%; background-color:${ this.pokemon.stats[1].base_stat/this.base >= 0.5 ? 'green' : 'gold'};">${this.pokemon.stats[1].base_stat}</div>
            </div>

            <div class="a_30 h_20 caja_vertical right p_0_15 m_10_0" >
              ${this.pokemon.stats[2].stat.name}
            </div>
            <div class="a_70 h_20 caja_vertical grey p_0 m_10_0">
            <div class="barra" style="height:20px;width:${this.pokemon.stats[2].base_stat/this.base*100}%; background-color:${ this.pokemon.stats[2].base_stat/this.base >= 0.5 ? 'green' : 'gold'};">${this.pokemon.stats[2].base_stat}</div>
            </div>

            <div class="a_30 h_20 caja_vertical right p_0_15 m_10_0" >
              ${this.pokemon.stats[3].stat.name}
            </div>
            <div class="a_70 h_20 caja_vertical grey p_0 m_10_0">
            <div class="barra" style="height:20px;width:${this.pokemon.stats[3].base_stat/this.base*100}%; background-color:${ this.pokemon.stats[3].base_stat/this.base >= 0.5 ? 'green' : 'gold'};">${this.pokemon.stats[3].base_stat}</div>
            </div>

            <div class="a_30 h_20 caja_vertical right p_0_15 m_10_0" >
              ${this.pokemon.stats[4].stat.name }
            </div>
            <div class="a_70 h_20 caja_vertical grey p_0 m_10_0">
            <div class="barra " style="height:20px;width:${this.pokemon.stats[4].base_stat/this.base*100}%; background-color:${ this.pokemon.stats[4].base_stat/this.base >= 0.5 ? 'green' : 'gold'};">${this.pokemon.stats[4].base_stat}</div>
            </div>
          
          
        </div>
        <div class="grey a_100 h_20" ></div>



      </div>
    `;
  }

  setgeneracion(e){
    if(e.target.value=="1"){
      this.limite="151";
      this.offset="0";
    }
    if(e.target.value=="2"){
      this.limite="100";
      this.offset="151";
    }
    if(e.target.value=="3"){
      this.limite="135";
      this.offset="251";
    }
    if(e.target.value=="4"){
      this.limite="107";
      this.offset="386";
    }
    if(e.target.value=="5"){
      this.limite="156";
      this.offset="493";
    }
    if(e.target.value=="6"){
      this.limite="156";
      this.offset="649";
    }
    if(e.target.value=="7"){
      this.limite="88";
      this.offset="721";
    }
    if(e.target.value=="8"){
      this.limite="89";
      this.offset="809";
    }
  }

  setPoke(e){
    this.pokemonId = e.target.value;
    this.dispatchEvent(new CustomEvent('seleccionPokemon', {
      detail: e.target.value,
      bubbles: true,
      composed: true,
    }));
    this.ocultar();
  }

  obtenerPokemon(){
    this.dispatchEvent(new CustomEvent('obtenerPokemon', {
      detail: {"limite":this.limite,"offset":this.offset},
      bubbles: true,
      composed: true,
    }));
    //prueba
    //this.llenarContenedor();
    //this.mostrar();
  }

  llenarContenedor(){
    let cont = this.shadowRoot.getElementById("contenedor");
    let select = this.shadowRoot.getElementById("select");
    this.matarHijos(); 
    let list=this.lista2;
      for (var i = 0; i < list.length; i++) {
        let divi = document.createElement("div");
        let option = document.createElement("bbva-web-form-option");
        option.value=list[i].name;
        option.innerHTML=list[i].name;
        select.appendChild(option);
        cont.appendChild(divi);

    }
    select.classList.remove("hidden");
  }

  matarHijos() {
    var list = this.shadowRoot.getElementById("select");
    while (list.hasChildNodes()) {
      list.removeChild(list.firstChild);
    }
  }

  mostrar(){
    let stats = this.shadowRoot.getElementById("stats");
    let art = this.shadowRoot.getElementById("art");
    let sprites = this.shadowRoot.getElementById("sprites");
    stats.classList.remove("hidden");
    art.classList.remove("hidden");
    sprites.classList.remove("hidden");
  }

  ocultar(){
    let stats = this.shadowRoot.getElementById("stats");
    let art = this.shadowRoot.getElementById("art");
    let sprites = this.shadowRoot.getElementById("sprites");
    stats.classList.add("hidden");
    art.classList.add("hidden");
    sprites.classList.add("hidden");
  }

  
}
