/* eslint-disable no-unused-vars */
import { css, unsafeCSS } from 'lit-element';

export default css`:

.caja{
  display:flex;
  flex-flow: row wrap;
  justify-content:center;
}
.a_100{
  width:100%;
}
.a_80{
  width:80%;
}
.a_90{
  width:90%;
}
.a_20{
  width:20%;
  margin:0.5px;
}
.a_30{
  width:30%;
}
.a_40{
  width:40%;
}
.a_60{
  width:60%;
}
.a_50{
  width:50%;
}
.a_70{
  width:70%;
}
.h_20{
  height:20px;
}
.caja2{
  display:flex;
  flex-flow: row wrap;
  justify-content:center;
 
}

.caja_vertical{
  display:flex;
  flex-flow: column wrap;

  padding:10px;
  
}

.space_eve{
  justify-content:space-evenly;
}

.center{
  justify-content:center;
}
.con_center{
  align-content:center;
}
.con_start{
  align-content:flex-start;
}

.blue{
  background: linear-gradient(to bottom, rgb(26,121,207), rgb(3,50,98));
}

.red{
  background-color:red;
}
.gold{
  background-color:gold;
}

.grey{
  background-color:#D8D6D5;
}
.azul{
  background-color:rgba(26,121,207,0.1);
}

.radio{
  border-radius:15px;
}

.right{
  align-items:right;
  text-align:right;
}

.box{
  box-sizing:border-box;
}

.hidden{
  display:none;
}

.p_0{
  padding:0px;
}
.p_0_15{
  padding:0px 15px;
}

.m_10_0{
  margin:10px 0px;
}

.barra{
  
  text-align:left;
  padding:0px 15px;
}

`;